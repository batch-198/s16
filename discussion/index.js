// alert("Hi");

// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Addition operator: " + sum);
//Result: 9228

let difference = y - x;
console.log("Subtraction operator: " + difference);

let product = x * y;
console.log("Multiplication operator: " + product);

let quotient = x / y;
console.log("Division operator: " + quotient);

let remainder = y % x;
console.log("Modulo operator: " + remainder);

// Assignment operator
	// Basic Assignment Operator (=)

let assignmentNumber = 8;

	// Addition Assignment Operator (+=)

// assignmentNumber = assignmentNumber + 2
// assignmentNumber = 8 + 2l

assignmentNumber += 2;
console.log("Addition assignment: " + assignmentNumber);
// Result: 10

assignmentNumber += 2;
console.log("Addition assignment: " + assignmentNumber);

let string1 = "Boston";
let string2 = "Celtics";
string1 += string2;
console.log(string1);
/* Result: BostonCeltics
string1 = string1 + string2

operator on the left side of the operation changes their value*/

assignmentNumber -= 2;
console.log("Subtraction assignment: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Multiplication assignment: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Division assignment: " + assignmentNumber);

// Multiple Operators and Parentheses
let mdas = 1 + 2 - 3 * 4 / 5
console.log("MDAS: " + mdas);
/* Result: 0.6
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6*/

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("PEMDAS: " + pemdas);
/* result: 0.199 or 0.2
	1. 4 / 5 = 0.8
	2. 2 - 3 = -1
	3. -1 * 0.8 = -0.8
	4. 1 + -0.8 = 0.2
*/

// Increment (+) and Decrement (-)
// always +1 or -1
let z = 1;

let increment = ++z;
console.log("Pre-increment: " + increment);
// Result: 2

console.log("Value of z: " + z);
// Result: 2

increment = z++;
console.log("Post-increment: " + increment);
// Result: 2

console.log("Value of z: " + z);
// Result: 3

/*post increment returns its original value before adding an increment*/

let decrement = --z;
console.log("Pre-decrement: " + decrement);
// Result: 2
console.log("Value of z: " + z);
// Result: 2

decrement = z--;
console.log("Post-decrement: " + decrement);
// Result: 2
console.log("value of z: " + z);
// Result: 1


// Type of Coercion
/*work for addition operator and equality operator*/
let numA = "10";
let numB= 12;

let coercion = numA + numB;
console.log(coercion);
// Result: 1012
console.log(typeof coercion);
// Result: String

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
// Result: 30
console.log(typeof nonCoercion);
// Result: Number

let numE = true + 1;
console.log(numE);
// Result: 2

let numF = false + 1;
console.log(numF);
// Result: 1


// Equality Operator (==)
console.log(1==1);
// Result: true
console.log(1==2);
// Result: false
console.log(1 == '1');
// Result: true
console.log(false == 0);
// Result: true
console.log('Johnny' == 'Johnny');
// Result: true
console.log('Johnny' == 'johnny');
// Result: false

// Inequality Operator (!=)

console.log (1 != 1);
// Result: false
console.log(1 != 2);
// Result: true
console.log(1 != '1');
// Result: false
console.log(0 != false);
// Result: false
console.log('Johnny' != 'Johnny');
// Result: false
console.log('Johnny' != 'johnny');
// Result: true

// Strict Equality Operator (===)
console.log(1 === 1);
// Result: true
console.log(1 === '1');
// Result: false (strict equality also compares the data type)
console.log('Johnny' === 'Johnny');
// Result: true

let johnny = 'johnny';
console.log('johnny' === johnny);
// Result: result

// Strict Inequality Operator (!==)
console.log(1 !== 1);
// Result: false
console.log(1 !== '1');
// Result: true
console.log('johnny' !== johnny)
// Result: false


// Relational Operator
let a = 50;
let b = 65;

// Greater Than (>)
let isGreaterThan = a > b;
console.log(isGreaterThan);
// Result: false

// Less Than (<)
let isLessThan = a < b;
console.log(isLessThan);
// Result: true

// Greater Than or Equal (>=)
let isGTE = a >= b;
console.log(isGTE);
// Result: False

// Less than or Equal (<=)
let isLTE = a <= b;
console.log(isLTE);
// Result: true

// forced coercion to change the string to a number
let numStr = '30';
console.log(a > numStr);
// Result: true

let str = 'twenty';
console.log(b >= str);
// result: false


// Logical Operators
	// AND Operators (&&)

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1);
// Result: false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2);
// Result: true

let authorization3 = isAdmin && isLegalAge;
console.log(authorization3)
// Result: false

let random = isAdmin && false;
console.log(random);

let requiredLevel = 95;
let requiredAge = 18;

let authorization4 = isRegistered && requiredLevel === 25;
console.log(authorization4);

let authorization5 = isRegistered && isLegalAge && requiredLevel == 95;
console.log(authorization5);
// Result: true

let userName = 'gamer2022';
let userName2 = 'theTinker';
let userAge = 15;
let userAge2 = 26;

let registration1 = userName.length > 8 && userAge >= requiredAge;
console.log(registration1);
// Result: false

// variable.length counts the characters

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2);
// Result: true

// OR Operator (|| - double pipe)

let userLevel = 100;
let userLevel2 = 65;

let guildRequirement = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge;
console.log(guildRequirement);
// Result: false

let guildRequirement2 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirement2);

let guildRequirement3 = userLevel >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement3);

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin);

// Not Operator
console.log(!isRegistered);
let opposite1 = !isAdmin;
console.log(opposite1);